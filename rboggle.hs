-- Reverse Boggle 4 x 4
{-
  Given a list of words found on a Boggle board, determine a possible
  board layout.

  To compile: ghc --make -O2 rboggle.hs

  To run: ./rboggle < wordlist
-}

import qualified Data.Map as Map

{-
  - Describe the positions on a grid
  - Find all possible paths within a grid
  - Generate all possible letter placements for a word
-}

-- Arbitrarily labeled positions in a 4x4 grid
data Position = A | B | C | D
              | E | F | G | H
              | I | J | K | L
              | M | N | O | P deriving (Eq, Enum, Ord, Show)

-- For a specified position, return a list of adjacent positions
adjacents :: Position -> [Position]
adjacents A = [B, E, F]
adjacents B = [A, C, E, F, G]
adjacents C = [B, D, F, G, H]
adjacents D = [C, G, H]
adjacents E = [A, B, F, I, J]
adjacents F = [A, B, C, E, G, I, J, K]
adjacents G = [B, C, D, F, H, J, K, L]
adjacents H = [C, D, G, K, L]
adjacents I = [E, F, J, M, N]
adjacents J = [E, F, G, I, K, M, N, O]
adjacents K = [F, G, H, J, L, N, O, P]
adjacents L = [G, H, K, O, P]
adjacents M = [I, J, N]
adjacents N = [I, J, K, M, O]
adjacents O = [J, K, L, N, P]
adjacents P = [K, L, O]

-- For a specified path, find all possible paths extended by one position
nextAdjacents :: [Position] -> [[Position]]
nextAdjacents []     = []
nextAdjacents (x:xs) = [adj : x : xs | adj <- adjacents x, adj `notElem` xs]

-- Generate all possible paths of a specified length (memoized)
paths :: Int -> [[Position]]
paths =
  let generatePaths 0 = []
      generatePaths 1 = map (: []) $ enumFrom A
      generatePaths x = concatMap nextAdjacents (paths (x-1))
  in (map generatePaths [0..] !!)

-- Generate one quarter of all possible paths of a specified length by
-- starting only from positions in one quarter of the grid
-- Note: this function is very similar to the paths function, but 
-- constitutes an optimization allowable for one target word (the longest)
quarterPaths :: Int -> [[Position]]
quarterPaths 0 = []
quarterPaths 1 = [[A],[B],[E],[F]]
quarterPaths x = concatMap nextAdjacents (quarterPaths (x-1))

-- Generate all possible paths for a specific word using a specified
-- path generation function (paths or quarterPaths)
wordPaths :: String -> (Int -> [[Position]]) -> [[(Position, Char)]]
wordPaths word f = map (`zip` word) $ f $ length word

{-
  - Use a map of char by position to store letter placements
-}

-- Map of char by position with the "any" Char value ('?') for every Position
anysMap :: Map.Map Position Char
anysMap = Map.fromList $ zip (enumFrom A) (repeat '?')

-- Update a map of char by position from a (Position, Char) tuple
mapChar :: Map.Map Position Char -> (Position, Char) -> Map.Map Position Char
mapChar m (p, c) = Map.insert p c m

{-
  - Define a trie data type to store possible placements for a word
  - This data type allows invalid combinations to be discarded quickly
  - Insertion function builds a trie
  - Walk function searches a trie, avoiding incompatible subtries
  - Map of char by position used to indicate preexisting placements
-}

data Trie = Empty | Trie Position [(Char, Trie)] deriving Show

-- Insert a string into a Trie
tinsert :: String -> Trie -> Trie
tinsert cs t = tinsert' cs (enumFrom A) t
  where
    tinsert' []     _      t           = t
    tinsert' (c:cs) (p:ps) Empty       = Trie p [(c, tinsert' cs ps Empty)]
    tinsert' (c:cs) (p:ps) (Trie _ xs) = 
      case tfind c xs [] of
        Nothing       -> Trie p ((c, tinsert' cs ps Empty) : xs)
        Just (t', ys) -> Trie p ((c, tinsert' cs ps t') : ys)

-- Determine whether a specified character already exists in a list of
-- Char, Trie tuples. If so, return a tuple of its subtrie and a list of 
-- the other Char, Trie tuples in the list; otherwise return Nothing.
tfind :: Char -> [(Char, Trie)] -> [(Char, Trie)] -> Maybe (Trie, [(Char, Trie)])
tfind c []     acc = Nothing
tfind c (x:xs) acc = 
  case tfind' c x of
    Nothing -> tfind c xs (x : acc)
    Just t  -> Just (t, acc ++ xs)
  where tfind' c (c', t') = if c == c' then Just t' else Nothing

-- Create a map of char by position from a list of (Position, Char) tuples
generateMap :: [(Position, Char)] -> Map.Map Position Char
generateMap = foldl mapChar anysMap

-- Extract an ordered list of chars from a map of char by position
mapToString :: Map.Map Position Char -> String
mapToString = Map.elems

-- Create a trie for a specified list of chars and path generation function
mkTrie :: String -> (Int -> [[Position]]) -> Trie
mkTrie word f = foldr (tinsert . mapToString . generateMap) Empty (wordPaths word f)

-- Given a trie and a starting map, return a list of all consistent submaps 
-- found within the trie
twalk :: Trie -> Map.Map Position Char -> [Map.Map Position Char]
twalk Empty       m = [m]
twalk (Trie p xs) m = concat $ twalk' xs p m
  where
    twalk' []     _ _ = []
    twalk' (x:xs) p m = maybeCompare x (Map.lookup p m) p m : twalk' xs p m
    -- Deal with Maybe return value of Map.lookup
    maybeCompare x Nothing   p m = []
    maybeCompare x (Just mc) p m = compareChars x mc p m
    -- Compare value of chars in the starting map and in the trie
    compareChars ('?', t) _   p m = twalk t m
    compareChars (a,   t) '?' p m = twalk t (Map.insert p a m)
    compareChars (a,   t) b   p m = if a == b then twalk t m else []

-- Concatenate the outputs from walking a trie with multiple maps
traverse :: Trie -> [Map.Map Position Char] -> [Map.Map Position Char]
traverse t = concatMap (twalk t)

{-
  User interface and high level application code
-}

-- Print out the contents of a map
showMap :: Map.Map Position Char -> IO ()
showMap m = do
    putStrLn $ take 4 $ Map.elems m
    putStrLn $ take 4 $ drop 4 $ Map.elems m
    putStrLn $ take 4 $ drop 8 $ Map.elems m
    putStrLn $ take 4 $ drop 12 $ Map.elems m

-- Sort a list of strings by string length
sortByLength :: Ord a => [[a]] -> [[a]]
sortByLength []     = []
sortByLength (x:xs) = [y | y <- xs, length y > length x] ++
                      [x] ++
                      [y | y <- xs, length y <= length x]

-- Given a list of words, generate a list of solution maps
-- Optimization #1: for longest word use only one quarter of starting positions
-- Optimization #2: for next longest word use generated maps rather than trie
-- Process tries from smallest to largest
wordListToMaps :: [String] -> [Map.Map Position Char]
wordListToMaps words = 
  foldr traverse baseMapList $ quarterTrie : restTries
    where
      quarterTrie = mkTrie (head words) quarterPaths
      baseMapList = map generateMap $ wordPaths (words !! 1) paths
      restTries = map (`mkTrie` paths) $ reverse $ drop 2 words

main = do
  wordLines <- getContents
  showMap $ head $ wordListToMaps $ sortByLength $ lines wordLines
